/*
 * MIT License
 *
 * Copyright (c) 2017 Boris
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.mycompany.javaheaderupdater;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class HeaderService {

  private final List<String> licenseLines;
  private int numberOfFiles;

  public HeaderService(final Path licenseFile) {
    this.licenseLines = new FileLines(licenseFile).get();
  }

  public int getNumberOfFilesUpdated() {
    return numberOfFiles;
  }

  public void updateJavaHeaders(final Path repoDirectory) {
    Stream<Path> files = new AllFiles("java").listRecursively(repoDirectory);
    updateJavaHeaders(files);
  }

  private void updateJavaHeaders(final Stream<Path> files) {
    files.parallel()
        .forEach(file -> {
          updateJavaHeader(file);
          countNumberOfFilesUpdated();
        });
  }

  private void updateJavaHeader(final Path file) {
    try {
      Files.write(file, getNewLines(file));
    } catch (IOException ex) {
      throw new UncheckedIOException(ex);
    }
  }

  private List<String> getNewLines(final Path file) {
    List<String> linesWithoutCopyrightHeader = new JavaClass(file).getWithoutCopyrightHeader();
    List<String> newLines = new ArrayList<>();
    newLines.addAll(new CopyrightHeader(licenseLines).get());
    newLines.add("");
    newLines.addAll(linesWithoutCopyrightHeader);
    return newLines;
  }

  private void countNumberOfFilesUpdated() {
    numberOfFiles++;
  }

}
