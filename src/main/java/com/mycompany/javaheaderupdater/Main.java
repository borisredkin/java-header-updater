/*
 * MIT License
 *
 * Copyright (c) 2017 Boris
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.mycompany.javaheaderupdater;

import java.nio.file.Paths;
import org.apache.commons.io.FilenameUtils;

public class Main {

  public static void main(String[] args) {
    if (args.length != 2) {
      System.err.println("Usage: java -jar java-header-updater-1.0.1.jar directory LICENSE.txt");
      System.exit(-1);
    }

    HeaderService copyrightService = new HeaderService(Paths.get(FilenameUtils.getName(args[1])));
    copyrightService.updateJavaHeaders(Paths.get(FilenameUtils.getName(args[0])));
    System.out.println("Files updated: " + copyrightService.getNumberOfFilesUpdated());
  }

}
