[![][sonar img]][sonar]
# README #

Java Header Updater updates java headers from a file containing the required header.

### Usage ###

To update all java files in a directory using a license file as a header, run:
```
#!

$ java -jar java-header-updater-1.0.7.jar directory LICENSE.txt
```

NOTE: don't include end separator in the directory path

### Latest version ###

 - **1.0.7** Updated to Java 13

* [Download](https://bitbucket.org/borisredkin/java-header-updater/downloads/java-header-updater-1.0.7.jar)

### Old versions ###

 - **1.0.6** Updated to Java 12

 - **1.0.5** Removed VersionEye and updated dependencies
 
 - **1.0.4** Added code coverage and updated build configuration
 
 - **1.0.3** Updated dependencies of the project
 
 - **1.0.2** Added [VersionEye](https://www.versioneye.com/) and SonarQube to the project

 - **1.0.1** Single asterisk in copyright header

 - **1.0.0** Initial version

### Who do I talk to? ###

[![](https://stackexchange.com/users/flair/4007082.png)](https://stackoverflow.com/users/story/3301492)

[sonar]:https://sonarcloud.io/dashboard?id=com.mycompany:java-header-updater
[sonar img]:https://sonarcloud.io/api/project_badges/measure?project=com.mycompany%3Ajava-header-updater&metric=alert_status